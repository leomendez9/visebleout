//
//  APIConfig.swift
//  Visebleout
//
//  Created by Leonardo Mendez on 23/03/22.
//

import Foundation

enum APIConfig: String {
    
    case develop
    
    var url: String {
        switch self {
        case .develop: return "https://jsonplaceholder.typicode.com"
        }
    }
    
}
