//
//  Photo.swift
//  Visebleout
//
//  Created by Leonardo Mendez on 23/03/22.
//

import UIKit
import ObjectMapper

struct Photo: Mappable {
    
    public var albumId: Int = 0
    public var id: String = ""
    public var title: String = ""
    public var url: String = ""
    public var thumbnailUrl: String = ""
    
    public init?(map: Map) {}
    public init() {}
    
    public mutating func mapping(map: Map) {
        albumId <- map["albumId"]
        id <- map["id"]
        title <- map["title"]
        url <- map["url"]
        thumbnailUrl <- map["thumbnailUrl"]
    }
    
}
