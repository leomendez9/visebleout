//
//  Comment.swift
//  Visebleout
//
//  Created by Leonardo Mendez on 23/03/22.
//

import UIKit
import ObjectMapper

struct Comment: Mappable {
    
    public var postId: Int = 0
    public var id: String = ""
    public var name: String = ""
    public var email: String = ""
    public var body: String = ""
    
    public init?(map: Map) {}
    public init() {}
    
    public mutating func mapping(map: Map) {
        postId <- map["postId"]
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        body <- map["body"]
    }
    
}
