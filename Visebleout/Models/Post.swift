//
//  Post.swift
//  Visebleout
//
//  Created by Leonardo Mendez on 23/03/22.
//

import UIKit
import ObjectMapper

struct Post: Mappable {
    
    public var userId: Int = 0
    public var id: String = ""
    public var title: String = ""
    public var body: String = ""
    
    public init?(map: Map) {}
    public init() {}
    
    public mutating func mapping(map: Map) {
        userId <- map["userId"]
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
    }
    
}
