//
//  PostStore.swift
//  Visebleout
//
//  Created by Leonardo Mendez on 23/03/22.
//

import UIKit
import Alamofire
import ObjectMapper

struct PostStore {
    
    func fecthPost(_ completion: @escaping(_ post: [Post]?, _ message: String) -> Void) {
        guard let url = URL(string: "\(APIConfig.develop.url)/posts") else { return }
        AF.request(url, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                if let value = value as? [[String: Any]] {
                    let posts = Mapper<Post>().mapArray(JSONArray: value)
                    DispatchQueue.main.async {
                        completion(posts, "")
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil, "error")
                    }
                }
            case .failure(let error):
                print(error)
                completion(nil, "error")
            }
        }
    }
    
}
